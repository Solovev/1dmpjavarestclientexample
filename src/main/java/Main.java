import java.util.List;

import entity.AuthResponse;
import entity.Job;
import entity.JobFilter;
import entity.JobStatistic;
import entity.JobType;
import entity.Resource;

/**
 * User: Constantine Solovev
 * Created: 01.06.15  17:51
 */


public class Main {


    public static void main(String[] args) {

        RestClient client = new RestClient();

        AuthResponse authentication = client.authenticate("your login", "tour password");
        String token = authentication.getAccessToken();

        Resource webdav = client.createResource(
            Resource.forWebDav("resourceName 1", "http://webdav.localhost.com/folder1", "webdavUser", "webdavPassword"),
            token
        );
        Resource hdfs = client.createResource(Resource.forHdfs("resourceName 2", "folder1"), token);

        Job job = client.createJob(Job.forRequest("transfer", JobType.DOWNLOAD, hdfs.getId(), webdav.getId()), token);

        client.enableJob(job.getId(), token);

        // статистики пока еще нет, она появится после завершения выполнения задачи.
        List<JobStatistic> jobStatistic = client.getJobStatistics(new JobFilter(job.getId()),  token);

    }

}
