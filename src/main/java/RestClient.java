import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import entity.AuthRequest;
import entity.AuthResponse;
import entity.Job;
import entity.JobFilter;
import entity.JobRequest;
import entity.JobStatistic;
import entity.JobStatus;
import entity.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * User: Constantine Solovev
 * Created: 01.06.15  19:04
 */


public class RestClient {

    RestTemplate rt = new RestTemplate();

    private static String rootUrl = "https://api.1dmp.io/";
    private static String authUrl = rootUrl + "v0.5/auth/token";
    private static String resourceUrl = rootUrl + "v0.5/depository/endpoints";
    private static String createJobUrl = rootUrl + "v0.5/depository/jobs";
    private static String enabledJobUrlTemplate = rootUrl + "v0.5/depository/jobs/%s/status";
    private static String jobStatisticsUrl = rootUrl + "v0.5/statistics/job/stats";

    public RestClient() {

        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        messageConverters.add(new MappingJackson2HttpMessageConverter());
        rt.setMessageConverters(messageConverters);

    }

    public AuthResponse authenticate(String username, String password) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<AuthRequest> request = new HttpEntity<>(new AuthRequest(username, password), headers);
        ResponseEntity<AuthResponse> responseEntity = rt.postForEntity(authUrl, request, AuthResponse.class);

        return responseEntity.getBody();
    }

    public Resource createResource(Resource requestBody, String token) {
        HttpEntity<Resource> request = createRequest(requestBody, token);

        ResponseEntity<Resource> responseEntity = rt.postForEntity(resourceUrl, request, Resource.class);
        return responseEntity.getBody();
    }

    public Job createJob(JobRequest job, String token) {
        HttpEntity<JobRequest> request = createRequest(job, token);
        ResponseEntity<Job> responseEntity = rt.postForEntity(createJobUrl, request, Job.class);
        return responseEntity.getBody();
    }

    public void enableJob(String jobId, String token) {
        HttpEntity<JobStatus> request = createRequest(new JobStatus("started"), token);
        rt.put(String.format(enabledJobUrlTemplate, jobId), request);
    }

    public List<JobStatistic> getJobStatistics(JobFilter filter, String token) {
        HttpHeaders header = composeAuthHeader(token);

        ResponseEntity<List> responseEntity = rt.exchange(
                jobStatisticsUrl,
                HttpMethod.GET, new HttpEntity<>(header),
                List.class,
                Collections.singletonMap("jobId", filter.getJobId())
        );
        return responseEntity.getBody();
    }

    /* private methods */

    private <T> HttpEntity<T> createRequest(T body, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAll(composeAuthHeader(token).toSingleValueMap());
        return new HttpEntity<>(body, headers);
    }

    private HttpHeaders composeAuthHeader(String token) {
        HttpHeaders bearer = new HttpHeaders();
        bearer.put("Authorization", Arrays.asList("bearer " + token));
        return bearer;
    }

}
