package entity;

/**
 * User: Constantine Solovev
 * Created: 01.06.15  22:22
 */


public class JobStatistic {

    private Long jobTypeId;
    private Long countTotal;
    private Long sizeTotal;

    public Long getJobTypeId() {
        return jobTypeId;
    }

    public void setJobTypeId(Long jobTypeId) {
        this.jobTypeId = jobTypeId;
    }

    public Long getCountTotal() {
        return countTotal;
    }

    public void setCountTotal(Long countTotal) {
        this.countTotal = countTotal;
    }

    public Long getSizeTotal() {
        return sizeTotal;
    }

    public void setSizeTotal(Long sizeTotal) {
        this.sizeTotal = sizeTotal;
    }
}
