package entity;

/**
 * User: Constantine Solovev
 * Created: 01.06.15  19:40
 */


public class Resource {

    private String id;
    private String created;
    private String name;
    private String address;
    private String username;
    private String password;
    private String packer;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPacker() {
        return packer;
    }

    public void setPacker(String packer) {
        this.packer = packer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static Resource forWebDav(String name, String address, String login, String password) {
        Resource resource = new Resource();
        resource.setName(name);
        resource.setAddress(address);
        resource.setType("webdav");
        resource.setUsername(login);
        resource.setPassword(password);
        return resource;
    }

    public static Resource forHdfs(String name, String address) {
        Resource resource = new Resource();
        resource.setName(name);
        resource.setType("hdfs");
        resource.setAddress(address);
        return resource;
    }
}
