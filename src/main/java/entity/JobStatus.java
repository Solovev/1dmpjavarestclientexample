package entity;

/**
 * User: Constantine Solovev
 * Created: 01.06.15  22:03
 */


public class JobStatus {

    private String status;


    public JobStatus() {
    }

    public JobStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
