package entity;

/**
 * User: Constantine Solovev
 * Created: 01.06.15  20:57
 */


public class IdEntity {

    private Long id;

    public IdEntity() { }

    public IdEntity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
