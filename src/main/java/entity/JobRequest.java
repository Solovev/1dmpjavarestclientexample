package entity;

/**
 * User: Constantine Solovev
 * Created: 01.06.15  21:47
 */


public class JobRequest {

    private String name;
    private String jobTypeId;
    private IdEntity sourceEndpoint;
    private IdEntity destinationEndpoint;

    public JobRequest() {
    }

    public JobRequest(String name, String type, IdEntity sourceEndpoint, IdEntity destinationEndpoint) {
        this.name = name;
        this.jobTypeId = type;
        this.sourceEndpoint = sourceEndpoint;
        this.destinationEndpoint = destinationEndpoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJobTypeId() {
        return jobTypeId;
    }

    public void setJobTypeId(String jobTypeId) {
        this.jobTypeId = jobTypeId;
    }

    public IdEntity getSourceEndpoint() {
        return sourceEndpoint;
    }

    public void setSourceEndpoint(IdEntity sourceEndpoint) {
        this.sourceEndpoint = sourceEndpoint;
    }

    public IdEntity getDestinationEndpoint() {
        return destinationEndpoint;
    }

    public void setDestinationEndpoint(IdEntity destinationEndpoint) {
        this.destinationEndpoint = destinationEndpoint;
    }
}
