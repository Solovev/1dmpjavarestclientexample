package entity;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * User: Constantine Solovev
 * Created: 01.06.15  20:30
 */


@JsonIgnoreProperties(ignoreUnknown = true)
public class Job {

    private String id;
    private String name;
    private String jobTypeId;
    private String created;
    private IdEntity sourceEndpoint;
    private IdEntity destinationEndpoint;
    private String status;
    private Map<String, Object> params;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJobTypeId() {
        return jobTypeId;
    }

    public void setJobTypeId(String jobTypeId) {
        this.jobTypeId = jobTypeId;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public IdEntity getSourceEndpoint() {
        return sourceEndpoint;
    }

    public void setSourceEndpoint(IdEntity sourceEndpoint) {
        this.sourceEndpoint = sourceEndpoint;
    }

    public IdEntity getDestinationEndpoint() {
        return destinationEndpoint;
    }

    public void setDestinationEndpoint(IdEntity destinationEndpoint) {
        this.destinationEndpoint = destinationEndpoint;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static JobRequest forRequest(
        String name,
        String jobTypeId,
        String sourceEndpoint,
        String destinationEndpoint
    ) {
        JobRequest job = new JobRequest();
        job.setName(name);
        job.setJobTypeId(jobTypeId);
        job.setSourceEndpoint(new IdEntity(Long.parseLong(sourceEndpoint)));
        job.setDestinationEndpoint(new IdEntity(Long.parseLong(destinationEndpoint)));
        return job;
    }
}
