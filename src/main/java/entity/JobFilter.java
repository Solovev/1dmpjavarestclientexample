package entity;

/**
 * User: Constantine Solovev
 * Created: 01.06.15  22:17
 */


public class JobFilter {

    private String from;
    private String to;
    private String jobId;
    private String jobType;

    public JobFilter() {
    }

    public JobFilter(String jobId) {
        this.jobId = jobId;
    }

    public JobFilter(String from, String to, String jobId, String jobType) {
        this.from = from;
        this.to = to;
        this.jobId = jobId;
        this.jobType = jobType;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }
}
