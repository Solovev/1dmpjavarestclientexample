package entity;

/**
 * User: Constantine Solovev
 * Created: 02.06.15  20:15
 */


/*
 * Copyright (c) CJSC IT Solutions Company "CleverDATA", www.cleverdata.ru
 *
 * All Rights Reserved
 */

import java.io.Serializable;

public class JobType implements Serializable {

    /**
     * Crawling job type id.
     */
    public static final String CRAWLING = "1";

    /**
     * Download job type id.
     */
    public static final String DOWNLOAD = "2";

    /**
     * Text classification job type id.
     */
    public static final String TEXTCLASSIFICATION = "3";

    /**
     * Transfer job type id.
     */
    public static final String TRANSFER = "4";

    /**
     * Upload job type id.
     */
    public static final String UPLOAD = "5";

    /**
     * URL classification job type id.
     */
    public static final String URLCLASSIFICATION = "6";

    /**
     * Training job type id.
     */
    public static final String TRAINING = "7";


}
